#include <iostream>
#include <fstream>
#include <climits>

using namespace std;

void bellmanFord(int size, int **graph, ofstream &outputFile);

int main() {
    int ***inputGraph = nullptr;
    int *graphSizes = nullptr;
    int testCount, temp = 0;
    bool fromFile = false;

    ifstream inputFile("in", fstream::in);
    ofstream outputFile("out", fstream::out);
    if (inputFile.good()) {
        fromFile = true;
        inputFile >> testCount;
    } else {
        cin >> testCount;
    }
    inputGraph = new int **[testCount];
    graphSizes = new int[testCount];
    for (int testInsert = 0; testInsert < testCount; testInsert++) {
        if (fromFile) {
            inputFile >> graphSizes[testInsert];
        } else {
            cin >> graphSizes[testInsert];
        }
        inputGraph[testInsert] = new int *[graphSizes[testInsert]];
        for (int x = 0; x < graphSizes[testInsert]; x++) {
            inputGraph[testInsert][x] = new int[graphSizes[testInsert]];
            for (int y = 0; y < graphSizes[testInsert]; y++) {
                if (fromFile) {
                    inputFile >> temp;
                } else {
                    cin >> temp;
                }

                if (temp == 0) {
                    temp = INT_MAX;
                }
                inputGraph[testInsert][x][y] = temp;
            }
        }
    }
    if (fromFile) {
        inputFile.close();
    }

    for (int test = 0; test < testCount; test++) {
        bellmanFord(graphSizes[test], inputGraph[test], outputFile);
        outputFile << endl;
    }
    outputFile.close();

    return 0;
}

void bellmanFord(int size, int **graph, ofstream &outputFile) {
    int *resultD = nullptr;
    resultD = new int[size];

    resultD[0] = 0;
    for (int p = 1; p < size; p++) {
        resultD[p] = graph[0][p];
    }

    for (int p = 1; p < size; p++) { // I
        for (int y = 1; y < size; y++) {
            for (int x = 0; x < size; x++) { // II
                if (graph[y][x] == INT_MAX) {
                    continue;
                }
                if (resultD[y] != INT_MAX) {
                    if (resultD[x] > resultD[y] + graph[y][x]) {
                        resultD[x] = resultD[y] + graph[y][x];
                    }
                }
            }
        }
        for (int q = 0; q < size; q++) {
            if (resultD[q] != INT_MAX) {
                cout << resultD[q] << " ";
                outputFile << resultD[q] << " ";
                continue;
            }
            cout << 0 << " ";
            outputFile << 0 << " ";
        }
        cout << endl;
        outputFile << endl;
    }
}